package com.selenium.jira.updateTicket;

import org.bouncycastle.util.StoreException;
import org.openqa.selenium.By;

import com.gargoylesoftware.htmlunit.javascript.host.Document;
import com.selenium.jira.BaseTestUtils.Utils;

public class updateIssueImpl extends Utils {
	
	protected void update(String ticketId)
	{
	
	driver.findElement(By.id("quickSearchInput")).clear();
    driver.findElement(By.id("quickSearchInput")).sendKeys(ticketId);
    driver.findElement(By.cssSelector("input.hidden")).click();
	
    driver.findElement(By.id("edit-issue")).click();
    driver.findElement(By.id("summary")).clear();
    driver.findElement(By.id("summary")).sendKeys("Bug Summary");
    driver.findElement(By.id("description")).clear();
    driver.findElement(By.id("description")).sendKeys("Test Description");
    driver.findElement(By.id("environment")).clear();
    driver.findElement(By.id("environment")).sendKeys("Environment");
    driver.findElement(By.id("edit-issue-submit")).click();
	
	}

}
