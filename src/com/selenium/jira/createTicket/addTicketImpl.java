package com.selenium.jira.createTicket;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.openqa.selenium.By;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.codehaus.jackson.*;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.selenium.jira.BaseTestUtils.HttpClient;
import com.selenium.jira.BaseTestUtils.constantObj;
import com.selenium.jira.BaseTestUtils.Utils;

public class addTicketImpl extends Utils { 
	
	protected void addissue()
	{
		constantObj obj = new constantObj();
		
		driver.findElement(By.id("create_link")).click();
		
	    driver.findElement(By.xpath("//div[@id='project-single-select']/span")).click();
	    driver.findElement(By.id("project-field")).click();
	    driver.findElement(By.xpath("//div[@id='issuetype-single-select']/span")).click();
	    driver.findElement(By.linkText("Bug")).click();
	    driver.findElement(By.id("summary")).clear();
	    driver.findElement(By.id("summary")).sendKeys("summary");
	    driver.findElement(By.xpath("//div[@id='priority-single-select']/span")).click();
	    driver.findElement(By.linkText("Trivial")).click();
	    driver.findElement(By.cssSelector("span.aui-icon.icon-date")).click();
	    // ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
	    driver.findElement(By.xpath("//div[@id='assignee-single-select']/span")).click();
	    driver.findElement(By.linkText("Unassigned")).click();
	    driver.findElement(By.id("environment")).clear();
	    driver.findElement(By.id("environment")).sendKeys("Test");
	    driver.findElement(By.id("description")).clear();
	    driver.findElement(By.id("description")).sendKeys("Test Description");
	    driver.findElement(By.id("customfield_10064-2")).click();
	    driver.findElement(By.id("timetracking")).clear();
	    driver.findElement(By.id("timetracking")).sendKeys("4d");
	    driver.findElement(By.id("create-issue-submit")).click();
	}

	protected String issuesCount()
	{
		String NoOfissuesCount=null;
	
		driver.findElement(By.id("find_link")).click();
	    driver.findElement(By.id("filter_lnk_reported_lnk")).click();
	    driver.findElement(By.linkText("Advanced")).click();
	    driver.findElement(By.id("advanced-search")).clear();
	    
	  /*
	   * 
	   * Below is the query passing in the search box of the advanced setting 
	   * Same can be achieved if count for the current user can be obtained from db
	   *   
	   */
	    driver.findElement(By.id("advanced-search")).sendKeys("Query to get the count");

	    driver.findElement(By.xpath("//button[@type='button']")).click();
	    
	    return NoOfissuesCount;
	}
	
	public String hitSearchApi(String api) throws JsonParseException, JsonMappingException, IOException
	{
		HttpClient httpClient = new HttpClient();
		HttpGet httpGet = new HttpGet(api);
		httpGet.setHeader("Content-Type", "application/json");
		HttpResponse httpResponse = httpClient.sendRequest(httpGet);
		String response = httpClient.getResponse(httpResponse);
		Map<String, Object> params = new ObjectMapper().readValue(response, HashMap.class);           
		
		int total = Integer.parseInt(params.get("total").toString());
		
		//int total = httpResponse.getStatusLine().getStatusCode();
	
		System.out.println(api);
		System.out.println(httpResponse.toString());
		System.out.println(response);
		System.out.println(String.valueOf(total));
		return response;
	}
	
	public String search(String reporterName) throws JsonParseException, JsonMappingException, IOException
	{
		return hitSearchApi("https://jira.atlassian.com/rest/api/2/search?jql=reporter=" + reporterName);
	}
	
}
