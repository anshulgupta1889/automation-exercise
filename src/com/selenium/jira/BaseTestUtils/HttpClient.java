package com.selenium.jira.BaseTestUtils;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpClient 
{	
	public String getResponse(HttpResponse httpResponse)
	{
		StringBuffer response = null;

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					httpResponse.getEntity().getContent()));
			response = new StringBuffer();
			String line = "";
			while ((line = br.readLine()) != null) {
				response.append(line);
			}

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return response.toString();
	}
	
	public HttpResponse sendRequest(HttpUriRequest httpMethod)
	{
		HttpResponse httpResponse = null;
		try {
			httpResponse = new DefaultHttpClient().execute(httpMethod);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return httpResponse;
	}
}
