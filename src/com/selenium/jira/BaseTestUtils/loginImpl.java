package com.selenium.jira.BaseTestUtils;

import org.openqa.jetty.html.Link;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class loginImpl extends Utils {
	 private String baseUrl;
	
	protected void login()
	{
		//WebDriver driver=new FirefoxDriver();
		 baseUrl = BROWSER_URL;
		driver.get(baseUrl + "/browse/TST/?selectedTab=com.atlassian.jira.jira-projects-plugin:summary-panel");
	    driver.findElement(By.linkText("Log In")).click();
	    driver.findElement(By.id("username")).clear();
	    driver.findElement(By.id("username")).sendKeys(Utils.ADMIN_USER);
	    driver.findElement(By.id("password")).clear();
	    driver.findElement(By.id("password")).sendKeys(Utils.ADMIN_PASSWD);
	    driver.findElement(By.id("login-submit")).click();
	    	   
	}

}
