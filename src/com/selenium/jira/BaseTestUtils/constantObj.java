package com.selenium.jira.BaseTestUtils;

public class constantObj {
	
	protected enum summaryData {
		summary("Bug Summary"),
		environment("Environment"),
		description("Test Description"),
		timetracking("4d");
		private final String name;
		
		private summaryData(String name)
		{
			this.name = name;
		}
		
		public String toString()
		{
			return name;
		}
	}
	

}
