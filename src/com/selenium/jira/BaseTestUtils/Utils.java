package com.selenium.jira.BaseTestUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Driver;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;


public abstract class Utils {
	
	
	private static File configFile = new File("src/main/resources/config_jira.properties");
	private static BufferedReader reader = null;
	private static String line = null;
	protected static WebElement element= null;
	public static String BROWSER_URL;
	public static String ADMIN_USER;
	public static String ADMIN_PASSWD;
	protected static WebDriver driver;
	
	//loginPage loginJira= new loginPage();
	
	@BeforeSuite	
	public void browserSetup()
	{

	try
	{
		reader = new BufferedReader(new FileReader(configFile));
		while((line = reader.readLine()) != null)
		{
		if(line.contains("BROWSER_URL"))
		{
			BROWSER_URL = line.replaceAll("(.*)=(.*)", "$2").trim();
		}
		else if(line.contains("ADMIN_USER"))
		{
			ADMIN_USER = line.replaceAll("(.*)=(.*)", "$2").trim();
		}
		else if(line.contains("ADMIN_PASSWD"))
		{
			ADMIN_PASSWD = line.replaceAll("(.*)=(.*)", "$2").trim();
		}
		
		}
		driver=new FirefoxDriver();
	}
	catch(Exception e)
	{
		e.printStackTrace();
		}
	}
	
	@BeforeClass
	public void setUp() throws Exception
	
	{
		//driver = new FirefoxDriver();	 
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		loginPage loginJira= new loginPage();
		//driver.get(BROWSER_URL);
		loginJira.login();
	}
	
	protected enum summaryData {
		summary("Bug Summary"),
		environment("Environment"),
		description("Test Description"),
		timetracking("4d");
		private final String name;
		
		private summaryData(String name)
		{
			this.name = name;
		}
		
		public String toString()
		{
			return name;
		}
	}
	
}
