package com.selenium.jiraTicketCreateAndUpdate;

import java.util.List;

import com.gargoylesoftware.htmlunit.javascript.host.Document;
import com.selenium.jira.BaseTestUtils.Utils;
import com.selenium.jira.updateTicket.updateIssue;

import org.junit.After;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class updateJiraTicket extends Utils {

	String ticketId=null;
	String actualMessage[] =null;
	String expectedMessage = null;
	private StringBuffer verificationErrors = new StringBuffer();

	Document document = new Document();
	
	@BeforeClass
	public void updateTicket()
	{
		ticketId = "TST-57201";
		updateIssue Issue = new updateIssue();
		Issue.update(ticketId);
	}
	
	@Test
	public void afterUpdateTicket()
	{
		WebElement element = (WebElement) document.getElementsByTagName("activity-new-val");
		Select sel = new Select(element);
		List<WebElement> items = sel.getOptions();
	
		try
		{
		for(int i =1; i<items.size(); i++)
		{
			actualMessage[i] = items.get(i).getText().trim();
			Assert.assertEquals(actualMessage[0],summaryData.summary);
			Assert.assertEquals(actualMessage[0],summaryData.description);
			Assert.assertEquals(actualMessage[0],summaryData.environment);
		}
		}
		catch (Exception e) {
			// TODO: handle exception
		System.out.println(e);
		}
		
	}
	
	@After
	  public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	     Fail(verificationErrorString);
	    }
	}

	private void Fail(String verificationErrorString) {
		// TODO Auto-generated method stub
		
	}
}

