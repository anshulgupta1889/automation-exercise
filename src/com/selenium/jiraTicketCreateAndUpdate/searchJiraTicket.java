package com.selenium.jiraTicketCreateAndUpdate;

import java.util.NoSuchElementException;

import org.junit.After;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.selenium.jira.BaseTestUtils.Utils;
import com.selenium.jira.searchTicket.searchIssue;

public class searchJiraTicket extends Utils {
	
	String ticketId = null;
	String returnTicketId=null;
	private StringBuffer verificationErrors = new StringBuffer();
	private boolean acceptNextAlert = true;
	
	@Test
	public void searchTicket()
	{
		ticketId = "TST-57201";
		
		searchIssue searchIssues = new searchIssue();
		returnTicketId=searchIssues.search(ticketId);
		
		Assert.assertEquals(ticketId, returnTicketId);
	
	}

	@After
	  public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	     Fail(verificationErrorString);
	    }
	}
	private boolean isElementPresent(By by) {
	    try {
	      driver.findElement(by);
	      return true;
	    } catch (NoSuchElementException e) {
	      return false;
	    }
	  }

	  private boolean isAlertPresent() {
	    try {
	      driver.switchTo().alert();
	      return true;
	    } catch (NoAlertPresentException e) {
	      return false;
	    }
	  }

	  private String closeAlertAndGetItsText() {
	    try {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	      acceptNextAlert = true;
	    }
	  }

	private void Fail(String verificationErrorString) {
		// TODO Auto-generated method stub
		
	}
}
