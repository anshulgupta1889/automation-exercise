package com.selenium.jiraTicketCreateAndUpdate;

/*
 * Getting count before creating issue
 * Performing assert in case after creating issue in Jira to check whether count has been incremented or not for the currentUser 	
 * Passing the username for searching the ticket
 */


import java.io.IOException;
import java.util.NoSuchElementException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.After;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.selenium.jira.BaseTestUtils.Utils;
import com.selenium.jira.createTicket.addTicket;

public class createTicket extends Utils {
		private StringBuffer verificationErrors = new StringBuffer();
		private boolean acceptNextAlert = true;
		private String beforeCount=null;
		private String afterCount=null;

	@Test 
	public void createTicket() throws JsonParseException, JsonMappingException, IOException
	{	
			addTicket AddTickets = new addTicket();
			beforeCount = AddTickets.search("anshulgupta1889");
			System.out.println("Printing the count before adding the ticket" +beforeCount);
			AddTickets.addIssue();
			afterCount = AddTickets.search("anshulgupta1889");
		
			Assert.assertEquals(afterCount,beforeCount+1);			  
	}
	 
	@After
	  public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	     Fail(verificationErrorString);
	    }
	}
	private boolean isElementPresent(By by) {
	    try {
	      driver.findElement(by);
	      return true;
	    } catch (NoSuchElementException e) {
	      return false;
	    }
	  }

	  private boolean isAlertPresent() {
	    try {
	      driver.switchTo().alert();
	      return true;
	    } catch (NoAlertPresentException e) {
	      return false;
	    }
	  }

	  private String closeAlertAndGetItsText() {
	    try {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	      acceptNextAlert = true;
	    }
	  }

	private void Fail(String verificationErrorString) {
		// TODO Auto-generated method stub
		
	}

}

